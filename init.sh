#!/usr/bin/bash
apt-get install -y \
php7.3-fpm \
php7.3-cli \
php7.3-common \
php7.3-mysql \
php7.3-gd \
php7.3-json \
php7.3-xml \
php7.3-zip \
php7.3-curl \
php7.3-bz2 \
php7.3-intl \
php7.3-mbstring \
php7.3-ldap \
php-apcu \
redis-server \
php-redis \
php-imagick \
wget \
unzip \
nginx \
mariadb-server

# Change max_execution_time = 180 in /etc/php/7.3/fpm/php.ini