#!/usr/bin/bash

DIR_EXE=$(dirname $0)
THE_EXE=$(basename $0 .sh)

if [[ $1 == "" ]]
then
	echo "Please enter the host name of the new instance (ex: \`${THE_EXE}.sh mutu.girofle.cloud\`)"
	exit 1
fi

BASE_DIR="/var/www/"
INSTALL_DIR="${BASE_DIR}${1}/"
NEXTCLOUD_VERSION="18.0.4"
NEXTCLOUD_ARCHIVE_NAME="nextcloud-${NEXTCLOUD_VERSION}.tar.bz2"
NEXTCLOUD_ARCHIVE_LOCAL_PATH="${BASE_DIR}${NEXTCLOUD_ARCHIVE_NAME}"
NGINX_TEMPLATE_PATH=${DIR_EXE}/nginx.conf.tmpl
NGINX_CONF_FILE_PATH=/etc/nginx/sites-available/${1}.conf
NGINX_CONF_LINK_PATH=/etc/nginx/sites-enabled/${1}.conf

if [[ ! -f ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} ]]
then
	echo "Downloading Nextcloud ${NEXTCLOUD_VERSION} ..."
	wget -q -O ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} https://download.nextcloud.com/server/releases/${NEXTCLOUD_ARCHIVE_NAME}
fi

if [[ ! -f ${INSTALL_DIR}/index.php ]]
then
	echo "Creating new Nextcloud ${NEXTCLOUD_VERSION} instance at ${INSTALL_DIR} ..."
	mkdir -p ${INSTALL_DIR}
	tar -xf ${NEXTCLOUD_ARCHIVE_LOCAL_PATH} -C ${INSTALL_DIR} --strip-components=1
  chown -R www-data:www-data ${INSTALL_DIR}
fi

if [[ ! -f ${NGINX_CONF_LINK_PATH} ]]
then 
  echo "Creating nginx configuration file at ${NGINX_CONF_FILE_PATH} and link to it ${NGINX_CONF_LINK_PATH} ..."
	cp ${NGINX_TEMPLATE_PATH} ${NGINX_CONF_FILE_PATH}
	sed -i 's/INSTANCE_HOST_NAME/'${1}'/g' ${NGINX_CONF_FILE_PATH}
	ln -s ${NGINX_CONF_FILE_PATH} ${NGINX_CONF_LINK_PATH}
  echo "Nginx is reloading ..."
  systemctl reload nginx
fi

echo "New nextcloud instance ${1} has been created successfully."